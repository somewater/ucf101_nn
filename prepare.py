#!/usr/bin/env python3
import os

ROOT = os.path.dirname(os.path.abspath(__file__))
DATA = os.path.join(ROOT, 'data')
UCF_RAW = os.path.join(ROOT, 'data', 'UCF-101')
UCF_MP4 = os.path.join(ROOT, 'data', 'UCF-101-mp4')

if not os.path.isdir(UCF_RAW):
    if not os.path.isdir(DATA):
        os.mkdir(DATA)
    if not os.path.isfile('UCF101.rar'):
        print("Start loading UCF101 dataset...")
        os.system('wget "http://crcv.ucf.edu/data/UCF101/UCF101.rar"')
    print("Start extracting UCF101 dataset...")
    os.system('unrar x UCF101.rar %s' % DATA)

if not os.path.isdir(UCF_MP4):
    print("Start converting UCF101 dataset to MP4...")
    filepaths = []
    for label_dir in os.listdir(os.path.join(UCF_RAW)):
        for file in os.listdir(os.path.join(UCF_RAW, label_dir)):
            filepath = (UCF_RAW, label_dir, file)
            filepaths.append(filepath)
    files_len = len(filepaths)
    os.mkdir(UCF_MP4)
    for i, (_, label_dir, file_avi) in enumerate(filepaths):
        if file_avi.endswith('.avi'):
            file_mp4 = file_avi.rstrip('.avi') + '.mp4'
            input_filepath = os.path.join(UCF_RAW, label_dir, file_avi)
            output_filepath = os.path.join(UCF_MP4, label_dir, file_mp4)
            if not os.path.isfile(output_filepath):
                output_dir = os.path.join(UCF_MP4, label_dir)
                if not os.path.isdir(output_dir):
                    os.mkdir(output_dir)
                os.system('ffmpeg -v error -i %s %s' % (input_filepath, output_filepath))
        print("%d of %d files converted" % (i+1, files_len))

print("Dataset ready")

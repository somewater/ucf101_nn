package videonn;

import org.apache.commons.lang3.tuple.Pair;
import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.SequenceRecordReader;
import org.datavec.api.records.reader.impl.csv.CSVSequenceRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.api.split.NumberedFileInputSplit;
import org.datavec.codec.reader.BaseCodecRecordReader;
import org.datavec.codec.reader.NativeCodecRecordReader;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.AsyncDataSetIterator;
import org.deeplearning4j.datasets.iterator.ExistingDataSetIterator;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.api.specific.AVCMP4Adaptor;
import org.jcodec.common.SeekableDemuxerTrack;
import org.jcodec.common.io.FileChannelWrapper;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Picture;
import org.jcodec.containers.mp4.demuxer.MP4Demuxer;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UCF101Reader {
    public static final int V_WIDTH = 320;
    public static final int V_HEIGHT = 240;
    public static final int V_NFRAMES = 100;
    private final String dataDirectory;
    private volatile Map<Integer, String> _labelMap;

    public UCF101Reader(String dataDirectory) {
        this.dataDirectory = dataDirectory.endsWith("/") ? dataDirectory : dataDirectory + "/";
    }

    public DataSetIterator getDataSetIterator(int startIdx, int nExamples, int miniBatchSize) throws Exception {
        //testReadFrame();
//        SequenceRecordReader featuresTrain = getFeaturesReader(dataDirectory, startIdx, nExamples);
//        SequenceRecordReader labelsTrain = getLabelsReader(dataDirectory, startIdx, nExamples);
//
//        SequenceRecordReaderDataSetIterator sequenceIter =
//                new SequenceRecordReaderDataSetIterator(featuresTrain, labelsTrain, miniBatchSize, labelMap().size(), false);
//        sequenceIter.setPreProcessor(new VideoPreProcessor());

        ExistingDataSetIterator iter = new ExistingDataSetIterator(createDataSetIterable(startIdx, nExamples, miniBatchSize));
        // iter.setPreProcessor(new VideoPreProcessor());

        //AsyncDataSetIterator: Used to (pre-load) load data in a separate thread
        //return iter;
        return new AsyncDataSetIterator(iter,1);
    }

    private UCF101RecordIterable createDataSetIterable(int startIdx, int nExamples, int miniBatchSize) throws IOException {
        return new UCF101RecordIterable(dataDirectory, labelMap(), V_WIDTH, V_HEIGHT, startIdx, nExamples);
    }

    public Map<Integer, String> labelMap() throws IOException {
        if (_labelMap == null) {
            synchronized (this) {
                if (_labelMap == null) {
                    File root = new File(dataDirectory);
                    _labelMap = Files.list(root.toPath()).map(f -> f.getFileName().toString())
                            .sorted().collect(HashMap::new, (h, f) -> h.put(h.size(), f), (h, o) -> {});
                }
            }
        }
        return _labelMap;
    }

    public int fileCount() {
        return (int) UCF101RecordIterable.rowsStream(dataDirectory).count();
    }

    private class VideoPreProcessor implements DataSetPreProcessor {
        @Override
        public void preProcess(org.nd4j.linalg.dataset.api.DataSet toPreProcess) {
            toPreProcess.getFeatures().divi(255);  //[0,255] -> [0,1] for input pixel values
        }
    }

    private void testReadFrame() throws IOException, JCodecException {
        JCodecTest.FxShow show = new JCodecTest.FxShow();
        next:
        for (Iterator<Pair<Path, String>> iter = UCF101RecordIterable.rowsStream(dataDirectory).iterator();
             iter.hasNext(); ) {
            Pair<Path, String> pair = iter.next();
            Path path = pair.getKey();
            String label = pair.getValue();

            FileChannelWrapper in = NIOUtils.readableChannel(path.toFile());
            MP4Demuxer d1 = MP4Demuxer.createMP4Demuxer(in);
            SeekableDemuxerTrack videoTrack_ = (SeekableDemuxerTrack)d1.getVideoTrack();
            FrameGrab fg = new FrameGrab(videoTrack_, new AVCMP4Adaptor(videoTrack_.getMeta()));
            final int framesTotal = videoTrack_.getMeta().getTotalFrames();

            for (int i = 0; i < framesTotal; i++) {
                try {
                    Picture picture = fg.getNativeFrame();
                    //Picture picture = FrameGrab.getFrameFromFile(path.toFile(), i);
                    show.accept(picture);
                } catch (Throwable ex) {
                    System.out.println(ex.toString() + " frame " + i + " " + path.toString());
                    continue next;
                }
            }

            in.close();
            System.out.println("OK " + path.toString());
        }
    }
}

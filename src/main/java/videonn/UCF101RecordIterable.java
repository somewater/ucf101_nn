package videonn;

import org.apache.commons.lang3.tuple.Pair;
import org.datavec.api.util.ndarray.RecordConverter;
import org.datavec.image.loader.ImageLoader;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.api.specific.AVCMP4Adaptor;
import org.jcodec.common.SeekableDemuxerTrack;
import org.jcodec.common.io.FileChannelWrapper;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Picture;
import org.jcodec.containers.mp4.demuxer.MP4Demuxer;
import org.jcodec.scale.AWTUtil;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.cpu.nativecpu.NDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.util.NDArrayUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class UCF101RecordIterable implements Iterable<DataSet> {
    private final String dataDirectory;
    private final Map<Integer, String> labelMap;
    private final int classesCount;
    private final Map<String, Integer> labelMapInversed;
    private final ImageLoader imageLoader;
    private final RecordReaderMultiDataSetIterator recordReaderMultiDataSetIterator;
    private final int skip;
    private final int limit;

    public UCF101RecordIterable(String dataDirectory, Map<Integer, String> labelMap, int rows, int cols, int skip, int limit) {
        this.dataDirectory = dataDirectory;
        this.labelMap = labelMap;
        this.labelMapInversed = new HashMap<>();
        for (Map.Entry<Integer, String> e : labelMap.entrySet()) {
            labelMapInversed.put(e.getValue(), e.getKey());
        }
        imageLoader = new ImageLoader(rows, cols);
        classesCount = labelMap.size();
        recordReaderMultiDataSetIterator = new RecordReaderMultiDataSetIterator(labelMap.size(), imageLoader);
        this.skip = skip;
        this.limit = limit;
    }

    @NotNull
    @Override
    public Iterator<DataSet> iterator() {
        return rowsStream(dataDirectory).skip(this.skip).limit(this.limit).flatMap(p -> dataSetsStreamFromFile(p.getKey(), p.getValue())).iterator();
    }

    public static Stream<Pair<Path, String>> rowsStream(String dataDirectory) {
        try {
            List<Pair<Path, String>> files = Files.list(Paths.get(dataDirectory)).flatMap(dir -> {
                try {
                    return Files.list(dir).map(p -> Pair.of(p, dir.getFileName().toString()));
                } catch (IOException e) {
                    e.printStackTrace();
                    return Stream.empty();
                }
            }).collect(Collectors.toList());
            Collections.shuffle(files, new Random(43));
            return files.stream();
        } catch (IOException e) {
            e.printStackTrace();
            return Stream.empty();
        }
    }

    private Stream<DataSet> dataSetsStreamFromFile(Path path, String label) {
        // return Stream.of(dataSetsIteratorFromFile(path, label).next());
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(dataSetsIteratorFromFile(path, label), Spliterator.ORDERED), false);
    }

    private Iterator<DataSet> dataSetsIteratorFromFile(Path path, String label) {
        FileChannelWrapper _in = null;
        INDArray labelArray = labelToNdArray(label);
        try {
            _in = NIOUtils.readableChannel(path.toFile());
            MP4Demuxer d1 = MP4Demuxer.createMP4Demuxer(_in);
            SeekableDemuxerTrack videoTrack_ = (SeekableDemuxerTrack)d1.getVideoTrack();
            FrameGrab fg = new FrameGrab(videoTrack_, new AVCMP4Adaptor(videoTrack_.getMeta()));
            //fg.seekToFramePrecise(frameNumber).getNativeFrame();
            final int framesTotal = videoTrack_.getMeta().getTotalFrames();
            FileChannelWrapper final_in = _in;
            return Collections.singleton(recordReaderMultiDataSetIterator.nextDataSet(_in, framesTotal, fg, labelMapInversed.get(label), labelMap.size())).iterator();
            //return new FrameIterator(framesTotal, _in, fg, labelArray, imageLoader);
        } catch (IOException | JCodecException e) {
            e.printStackTrace();
            return Collections.emptyIterator();
        }
    }

    private INDArray labelToNdArray(String label) {
        int maxTSLength = 1; // frames per dataset
        int labelVal = labelMapInversed.get(label);
        INDArray arr = Nd4j.create(new int[]{1, classesCount/*, maxTSLength*/}, 'f');
        arr.put(0, labelVal, 1f);
        return arr;
    }

    private static class FrameIterator implements Iterator<DataSet> {
        private final int framesTotal;
        private final FrameGrab fg;
        private final ImageLoader imageLoader;
        private int currentFrame0 = 0;
        private DataSet dataSet = null;
        private final FileChannelWrapper final_in;
        private final INDArray labelArray;

        public FrameIterator(int framesTotal, FileChannelWrapper _in, FrameGrab fg, INDArray labelArray, ImageLoader imageLoader) {
            this.framesTotal = framesTotal;
            this.final_in = _in;
            this.fg = fg;
            this.labelArray = labelArray;
            this.imageLoader = imageLoader;
        }

        @Override
        public boolean hasNext() {
            if (dataSet != null) {
                return true;
            } else if (currentFrame0 < framesTotal) {
                tryReadFrame();
                if (dataSet == null) {
                    close();
                    return false;
                } else {
                    return true;
                }
            } else {
                close();
                return false;
            }
        }

        @Override
        public DataSet next() {
            DataSet ds = dataSet;
            dataSet = null;
            currentFrame0++;
            return ds;
        }

    private void tryReadFrame() {
        dataSet = null;
        Picture picture = null;
        try {
            fg.seekToFramePrecise(currentFrame0);
            picture = fg.getNativeFrame();
        } catch (IOException | JCodecException e) {
            e.printStackTrace();
            return;
        }
        dataSet = new DataSet(pictureToNdArray(picture), labelArray);
        currentFrame0++;
    }

    private void close() {
        if (final_in.isOpen()) {
            try {
                final_in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private INDArray pictureToNdArray(Picture picture) {
        return imageLoader.toRaveledTensor(AWTUtil.toBufferedImage(picture));
    }
}
}
